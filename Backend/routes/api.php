<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', [\App\Http\Controllers\LoginController::class, 'submit']);
Route::post('login/verify', [\App\Http\Controllers\LoginController::class, 'verify']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('driver', [\App\Http\Controllers\DriverController::class, 'show']);
    Route::post('driver', [\App\Http\Controllers\DriverController::class, 'update']);

    Route::post('trip', [\App\Http\Controllers\TripController::class, 'store']);
    Route::get('trip/{trip}', [\App\Http\Controllers\TripController::class, 'show']);
    Route::get('trip/{trip}/accept', [\App\Http\Controllers\TripController::class, 'accept']);
    Route::get('trip/{trip}/start', [\App\Http\Controllers\TripController::class, 'start']);
    Route::get('trip/{trip}/end', [\App\Http\Controllers\TripController::class, 'end']);
    Route::get('trip/{trip}/location', [\App\Http\Controllers\TripController::class, 'location']);

    Route::get('user', function (Request $request) {
        return $request->user();
    });
});


